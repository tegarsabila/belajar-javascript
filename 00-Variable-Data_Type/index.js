// INFO: Materi 00-Variable

// NOTE: 3 Tipe Variable

/* 1. var 
 * 2. let
 * 3 const
*/ 

// INFO: Tipe data
// 1. Primitif
// 2. Reference

// INFO: Jenis Tipe Data
// 1.1 Boolean ( true, false ) 
// 1.2 Number (n, -1, 0, 1, 2, n )
// 1.3 String ("Sapi", "Ayam", "Domba")
// 1.4 Null ( nilai yang tidak bernilai sama sekali )
// 1.5 Undefined ( sebuah variabel yang sudah dideklarasi tapi belum diisi )

let x = 6; // memasukan nilai 6 ke x
let y = 4; // memasukan nilai 4 ke y
let z = x + y; // memasukan nilai 10 ke z (6 + 4)
console.log(z);
