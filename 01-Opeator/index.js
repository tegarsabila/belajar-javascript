// INFO: Materi 01-Opetator

// NOTE: Jenis Opetator
// 1.1 Tambah (+)
// 1.2 Kurang (-)
// 1.3 Kali (*)
// 1.4 Bagi (/)
// 1.5 Pangkat (**)
// 1.6 Modulus/sisa bagi (%)
// 1.7 Auto penambahan(+1)/increment (++)
// 1.8 Auto pengurangan(-1)/decrement (--)
// TODO: Contoh
// Tambah
let a, b, c;
a = 3;
b = 8;
c = a + b; // 3 + 8 = 11 
console.log(c);
// Kurang
a = 6;
b = 2;
c = a - b; // 6 - 2 = 4 
console.log(c);
// Kali
a = 8;
b = 2;
c = a * b; // 8 * 2 = 16
console.log(c);
// Bagi
a = 10;
b = 5;
c = a / b; // 10 - 5 = 2
console.log(c);
// Pangkat
a = 4;
b = 2;
c = a ** b; // 4 pangkat 2 = 16
console.log(c);
// Modulus (Sisa bagi)
a = 10;
b = 4;
c = a % b; // 6 di bagi 2 = 2 sisa 2
console.log(c);
