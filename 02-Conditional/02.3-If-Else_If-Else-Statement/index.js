// INFO: Materi 02.3 If, Else If, Else Statement

// NOTE: Description:
// Pada If Statement kita hanya bisa membuat 1 kondisi saja, sedangkan dengan menggunakan If, Else If, Else Statement kita bisa membuat beberapa kondisi untuk menemtukan proses dimana dia akan di eksekusi ke dalam statement.

// TODO: Sytax If, Else If, Else Statement
/*
* if (kondisi 1) {
*   statement
* } else if (kondisi 2) {
*   statement
* } else {
*   statement
* }
*/

// TODO: Contoh Case
// Kita akan melanjutkan case sebelumnya, disini kita akan mengubah nilai umur menjadi -3, lalu kita akan menambahkan kondisi kedua untuk memfilter jika umur di bawah 1 tahun maka akan menampilkan tulisan "Data Invalid"

// PERF: Fix Case

// assign umur 
const umur = -3;

// If, Else If, Else Statement
if (umur >= 17) {
    console.log("Umur anda sudah mencukupi");
} else if (umur < 1) {
    console.log("Data Invalid");
} else {
    console.log("Anda belum cukup umur");
};

// selamat sampai sini kamu sudah bisa buat program untuk memfilter data umur
