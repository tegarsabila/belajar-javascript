// INFO: Materi 02.1 If Statement

// NOTE: Description:
// If Statement adalah penentuan sebuah kondisi, dimana jika kondisi itu sesuai maka dia akan di proses ke dalam statement nya, jika tidak sesuai maka statement tersebut tidak akan di jalankan

// TODO: Syntax if statement
/*
* if (kondisi) {
*   statement
* }
*/

// TODO: Contoh Case
// kita akan membuat sebuah variabel dengan nama umur lalu di isi nilai 18 dan akan di olah pada suatu kondisi untuk menentukan jika umur lebih dari atau sama dengan 17 maka akan menampilkan tulisan "Umur anda sudah mencukupi" sedangkan jika umur kurang dari 17 maka tidak akan di proses ke dalam statement

// PERF: Fix Case
// assign umur
const umur = 18;

// conditional if Statement
if (umur >= 17) {
    console.log("Umur anda sudah mencukupi");
}

