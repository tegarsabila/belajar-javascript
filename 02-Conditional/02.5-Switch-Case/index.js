// INFO: Materi 02.5 Switch Case 
// Special condition
// NOTE: Description:
// Switch Case pada dasarnya sama dengan pencabangan atau IF ELSE, hanya caranya yang berbeda, atau bisa dikatakan lebih sederhana dibanding dengan penggunaan IF ELSE.

// TODO: Syntax Switch Case
// switch (kondisi) {
//     case x:
//         statement
//         break;
//     case y:
//         statement
//         break;
//     default: // 
//         statement
// }
// break berguna untuk memberhentikan pengecekan jika hasil pengecekan suatu kasus sudah mendapatkan true

// TODO: Contoh Case
// Buatlah suatu variable string dengan nilai "javascript". Setelah itu buatlah Switch Case untuk membuat beberapa kasus, kasus pertama untuk mengecek apakah string nya bernilai "python", lalu kasus kedua untuk mengecek apakah string nya bernilai "golang" dan terakhir untuk mengecek apakah string bernilai "java", lalu tampilkan tulisan kasus di temukan di setiap masong-masing kasus, setelag itu buat kondisi default jika kasus tidak ada yang cocok dan tampilkan tulisan kasus tidak ditemukan.

// PERF: Fix Case 
// assign value javascript
let str = "javascript";

// Switch Case
switch (str) {
  case 'python':
    console.log('kasus ditemukan');
    break;
  case 'golang':
    console.log('kasus ditemukan');
    break;
  case 'java':
    console.log('kasus ditemukan');
    break;
  default:
    console.log('kasus tidak ditemukan');
    break;
}
