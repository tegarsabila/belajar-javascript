// INFO: Materi 02.4 Nested If

// NOTE: Description:
// Nested If adalah suatu kondisi bercabang, secara singkat nya yaitu ada kondisi di dalam kondisi, dimana jika kondisi kita sesuai kriteria, tetapi kondisi lagi di dalam nya maka kita akan terus di eksekusi sampai kita sesuai dengan semua kondisi percabangan nya, jika salah satu tidak sesuai maka dia akan di kembalikan ke awal mencari kondisi lain yang semua kondisi percabangan nya sesuai dengan kriteriakita.

// TODO: Syntax Nested If
/*
 * if (kondisi 1) {
 *   if (kondisi bercabang) {
 *       statement
 *   } else if (kondisi bercabang) {
 *       statement
 *   }
 * } else if (kondisi) {
 *   if (kondisi bercabang) {
 *       statement
 *   } else if (kondisi bercabang) {
 *       statement
 *   }
 * } else {
 *   statement
 * }
 */

// TODO: Contoh Case
// Buatlah program untuk mengecek tahap usia, dengan langkah awal membuat variable dengan nama umur dan nilai nya 18, lalu buat variable kosong dengan nama tahap. Selanjutnya buat sebuah kondisi untuk mengeksekusi jika umur kurang dari atau sama dengan 18 lalu buat kondisi percabangan untuk mengecek jika umur kurang dari atau sama dengan 10 maka ubah nilai variable tahap menjadi anak-anak jika tidak maka ubah menjadi remaja.
// lalu buat else untuk kondisi pertama yang di dalam nya terdapat kondisi untuk mengecek jika umur lebih besar atau sama dengan 65 maka tahap nya menjadi lansia jika tidak maka tahap menjadi dewasa.
// Jika sudah maka cetak lah variable tahap

// PERF: Fix Case

// assign umur
const umur = 60;
let tahap;

// Nested If Stetement
if (umur <= 18) {
    if (umur <= 10) {
        tahap = "anak - anak"
    } else {
        tahap = "remaja"
    }
} else {
    if (umur >= 65) {
        tahap = "lansia"
    } else {
        tahap = "dewasa"
    }
}

console.log(tahap);
