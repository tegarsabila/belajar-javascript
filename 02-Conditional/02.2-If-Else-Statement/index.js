// INFO: Materi 02.2 If Else Statement 

// NOTE: Description:
// If Else Statement sama seperti If statement, hanya dia memiliki sebuah pententuan akhir jika kondisi tidak ada yang sesuai maka dia akan di eksekusi pada kondisi akhir, secara singkat nya else ini adalah opsional dari kondisi-kondisi lainnya.

// TODO: Syntax If Else Statement 
/*
* if (kondisi) {
*   statement
* } else {
*   statement
* }
*/

// TODO: Contoh Case
// Case kali ini kita akan ambil pada case if statement sebelumnya hanya disini kita akan mengubah nilai umur menjadi 16 dan menambahkan eksekusi akhir jika umur di bawa 17 maka akan akan di tampilkan tulisan "Anda belum cukup umur"

// PERF: Fix Case

// assign umur
const umur = 16;

// Conditional If Else Statement
if (umur >= 17) {
    console.log("Umur anda sudah mencukupi"); // jika kondisi tidak sesuai maka akan di eksekusi oleh else
} else {
    console.log("Anda belum cukup umur");
};
