// INFO: Materi 03 Truthy and Falsy

// NOTE: Description:
// Truthy dan Falsy merupakan sebuah sebutan bagi nilai non-boolean yang dievaluasi menjadi boolean karena sifat automatic type conversion, atau coersion, dari Javascript. Nilai Truthy merupakan nilai non-boolean yang dievaluasi menjadi nilai true di Javascript. Sementara nilai Falsy merupakan nilai non-boolean yang dievaluasi menjadi nilai false di Javascript.

// TODO: Falsy
// Yang termasuk dalam Falsy
/*
* 1. false
* 2. 0 (nol)
* 3. "" (string kosong)
* 4. null
* 5. undefined
* 6. NaN
*/

// TODO: Truthy 
// Untuk mengetahui nilai apa saja yang bersifat Truthy maka harus memahami keenam nilai yang bersifat Falsy. Selebihnya, nilai apapun selain keenam nilai tersebut bersifat Truthy.

// TODO: Contoh

// Boolean
let bool1 = true; // TRUTHY 
let bool2 = false; // FALSY 

// Number
let numb1 = 0; // FALSY => NaN 
let numb2 = 9; // TRULY => Infinity 

// String
let str1 = 'Javascript'; // TRUTHY 
let str2 = ''; // FALSY

// undefined, null => FALSY 

if (!str2) {
  console.log('Code akan di jalankan');
}

// kondisi di atas akan menampilkan tulisan Code akan di jalankan, karena pada kondisi menyatakan bahwa apakah str2 tidak bernilai true?, karena str2 memiliki nilai bersifat Falsy maka pernyataan tersebut adalah benar dan akan di proses ke dalam statement nya.
